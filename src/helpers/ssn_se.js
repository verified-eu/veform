export const getIdNumberInfoSe = function(str) {
    let controlNumberValid = false
    let isPersonNumber = false
    let isSamordningsNumber = false
    let isMale = null
    let isFemale = null
    let isOrgNumberAndNotPnr = false
    let isOrgNumber = false
    let numsOnlyStr = str.replace(/[^\d]+/g, '')
    let numsStr = numsOnlyStr.slice(-10)
    if(numsStr.length == 10) {
        let sum = 0
        const l = numsStr.length;
        for (let i = 0; i < l; i++) {
            let v = parseInt(numsStr[i])
            v *= 2 - (i % 2)
            if (v > 9) {
                v -= 9
            }
            sum += v
        }
        const luhnValue = (10 - (sum % 10)) % 10
        controlNumberValid = luhnValue == 0 ? true : false
        if(controlNumberValid) {
            let year = parseInt(numsStr.slice(0, 2))
            let month = parseInt(numsStr.slice(2, 4))
            let day = parseInt(numsStr.slice(4, 6))
            let curDate = new Date()
            let curCentury = numsOnlyStr.length >= 12 ? numsOnlyStr.slice(-12,2): Math.floor(curDate.getFullYear()/100)*100
            let century = curCentury + year > curDate.getFullYear() ? curCentury -100 : curCentury
            let date = new Date(parseInt(century+year), month-1, day)
            isPersonNumber = !/invalid/i.test(date.toString()) && date.getFullYear() == century+year && date.getMonth() == month-1 && date.getDate() == day
            if(!isPersonNumber) {
                date = new Date(parseInt(century+year), month-1, day-60)
                isSamordningsNumber = !/invalid/i.test(date.toString())  && date.getFullYear() == century+year && date.getMonth() == month-1 && date.getDate() == day
            }
            if(isPersonNumber || isSamordningsNumber) {
                isMale = parseInt(numsStr.slice(-2, -1)) % 2 == 1
                isFemale = !isMale
            }
            isOrgNumberAndNotPnr = !/0|4/.test(numsStr.slice(0, 1)) && month >= 20
            isOrgNumber = isPersonNumber || isSamordningsNumber || isOrgNumberAndNotPnr
        }
    }
    return {
        controlNumberValid: controlNumberValid,
        isPersonNumber: isPersonNumber,
        isSamordningsNumber: isSamordningsNumber,
        isMale: isMale,
        isFemale: isFemale,
        isOrgNumberAndNotPnr: isOrgNumberAndNotPnr,
        isOrgNumber: isOrgNumber
    }
}
