import Header from './Header.vue'

export default {
    install(Vue) {
        Vue.component(Header.name, Header)
    }
}