import FormGroup from './FormGroup.vue'

export default {
    install(Vue) {
        Vue.component(FormGroup.name, FormGroup)
    }
}