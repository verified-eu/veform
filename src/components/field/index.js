import InputField from './InputField.vue'

export default {
    install(Vue) {
        Vue.component(InputField.name, InputField)
    }
}