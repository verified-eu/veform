import Headline from './Headline.vue'

export default {
    install(Vue) {
        Vue.component(Headline.name, Headline)
    }
}