# ve-headline

### Props
| Name     | Type   | Default   | Description                    |
|----------|--------|-----------|--------------------------------|
| headline | String | ""        | The headline text              |
| size     | String | ""        | Custom definition of font-size |
| tip      | Object | undefined | Defines the tooltip object     |

### Tooltip Props
| Name           | Type           | Default       | Description                                                                                                                                                                                         |
|----------------|----------------|---------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| html           | Boolean        | false         | If set to true, this prop allows HTML tags to be rendered in the tooltip. If false, jQuery's text method will be used to insert content into the DOM. Use text if you're worried about XSS attacks. |
| text           | String         | ""            | The label text                                                                                                                                                                                      |
| text-placement | String         | 'top'         | Defines the tooltip object                                                                                                                                                                          |
| tip-position   | String         | 'right'       | How to position the tooltip. Options: top, bottom, left, right.                                                                                                                                     |
| trigger        | String         | 'hover focus' | How tooltip is triggered. Options: click, hover, focus, manual.  You may pass multiple triggers by separating them with a space.  "manual" cannot be combined with any other trigger.               |
| offset         | String, Number | 0             | Offset of the tooltip relative to its target.                                                                                                                                                       |