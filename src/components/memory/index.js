import Memory from './Memory.vue'

export default {
    install(Vue) {
        Vue.component(Memory.name, Memory)
    }
}