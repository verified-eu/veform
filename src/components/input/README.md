# ve-input
...

# ve-textarea
...

# ve-radio
...

# ve-checkbox
...

# ve-select

### Props

| Name          | Type                              | Default | Description                                                                          |
|---------------|-----------------------------------|---------|--------------------------------------------------------------------------------------|
| disabled      | boolean                           | false   | Disables the input field                                                             |
| options       | string[] or object<text, value>[] | null    | The different options of the select                                                  |
| searchable    | boolean                           | false   | Whether or not it should be possible to search for the different options             |
| rules         | string                            | ""      | Validation rules                                                                     |
| icon          | string                            | null    | A fontawesome icon, e.g. "fas fa-wrench" that will be displayed in the search field. |
| icon-color    | string                            | "#333"  | Color of the icon                                                                    |
| loading       | boolean                           | false   | Whether or not to display a spinner/loader in the search field                       |
| ignore-filter | boolean                           | false   | Show all options even when something is entered in the search field                  |
| placeholder   | string                            | ""      | Overwritea the default text shown in the input                                       |

### Events

| Name      | Type             | Description                                                 |
|-----------|------------------|-------------------------------------------------------------|
| on-select | string or object | Event is dispatched when the user clicks on an option       |
| on-search | string           | Event is dispatched when the user types in the search input |

# ve-brreg
...

# ve-dropdown
Deprecated, use ve-select instead.

