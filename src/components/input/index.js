import Input from './Input.vue'
import Brreg from './Brreg.vue'
import Dropdown from './Dropdown.vue'
import Select from './Select.vue'
import Textarea from './Textarea.vue'
import Radio from './Radio.vue'
import Checkbox from './Checkbox.vue'
import Datepicker from './Datepicker.vue'
import Switch from './Switch.vue'

export default {
    install(Vue) {
        Vue.component(Input.name, Input)
        Vue.component(Brreg.name, Brreg)
        Vue.component(Dropdown.name, Dropdown)
        Vue.component(Select.name, Select)
        Vue.component(Textarea.name, Textarea)
        Vue.component(Radio.name, Radio)
        Vue.component(Checkbox.name, Checkbox)
        Vue.component(Datepicker.name, Datepicker)
        Vue.component(Switch.name, Switch)
    }
}