# ve-counter

### Props
| Name   | Type   | Default | Description                                                             |
|--------|--------|---------|-------------------------------------------------------------------------|
| length | Number | null    | Sets the current length of the list object associated with the counter. |
| max    | Number | 10      | The max value of the counter.                                           |
| min    | Number | 0       | The min value of the counter.                                           |

### Events
| Name      | Type     | Description                                                        |
|-----------|----------|--------------------------------------------------------------------|
| on-add    | function | Event is dispatched when the user clicks on the "+" counter button |
| on-remove | function | Event is dispatched when the user clicks on the "-" counter button |