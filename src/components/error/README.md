# ve-error
Displays a full-page error modal from an array of exceptions or text.

Slot available for custom content.