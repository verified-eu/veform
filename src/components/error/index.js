import Error from './Error.vue'

export default {
    install(Vue) {
        Vue.component(Error.name, Error)
    }
}