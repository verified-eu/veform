import PDFViewer from './PDFViewer.vue'

export default {
    install(Vue) {
        Vue.component(PDFViewer.name, PDFViewer)
    }
}