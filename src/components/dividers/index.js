import Divider from './Divider.vue'

export default {
    install(Vue) {
        Vue.component(Divider.name, Divider)
    }
}