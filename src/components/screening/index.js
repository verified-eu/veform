import Screening from './Screening.vue'
import "./screening.scss"

export default {
    install(Vue) {
        Vue.component(Screening.name, Screening)
    }
}