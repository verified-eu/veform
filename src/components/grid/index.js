import Col from './Col.vue'
import Row from './Row.vue'

export default {
    install(Vue) {
        Vue.component(Col.name, Col)
        Vue.component(Row.name, Row)
    }
}