import Alert from './Alert.vue'

export default {
    install(Vue) {
        Vue.component(Alert.name, Alert)
    }
}