import Label from './Label.vue'

export default {
    install(Vue) {
        Vue.component(Label.name, Label)
    }
}