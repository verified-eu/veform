import header from './header'
import body from './body'
import label from './label'
import headline from './headline'
import form from './form'
import input from './input'
import field from './field'
import screening from './screening'
import stepper from './stepper'
import formgroup from './formgroup'
import grid from './grid'
import card from './card'
import counter from './counter'
import alert from './alert'
import memory from './memory'
import pdf from './pdf'
import loader from './loader'
import error from './error'

export const mount = (Vue) => {

    header.install(Vue)
    body.install(Vue)
    label.install(Vue)
    headline.install(Vue)
    form.install(Vue)
    input.install(Vue)
    field.install(Vue)
    screening.install(Vue)
    stepper.install(Vue)
    formgroup.install(Vue)
    grid.install(Vue)
    card.install(Vue)
    counter.install(Vue)
    alert.install(Vue)
    memory.install(Vue)
    pdf.install(Vue)
    loader.install(Vue)
    error.install(Vue)

}