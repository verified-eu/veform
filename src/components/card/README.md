# ve-card

### Props
| Name     | Type   | Default   | Description                                                                                              |
|----------|--------|-----------|----------------------------------------------------------------------------------------------------------|
| headline | String | undefined | Adds a header block on top of the card object and the header is set to the content of headline variable. |