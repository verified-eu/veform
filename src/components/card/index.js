import Card from './Card.vue'

export default {
    install(Vue) {
        Vue.component(Card.name, Card)
    }
}