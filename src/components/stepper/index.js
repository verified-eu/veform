import Stepper from './Stepper.vue'

export default {
    install(Vue) {
        Vue.component(Stepper.name, Stepper)
    }
}