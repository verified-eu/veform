import Form from './Form.vue'

export default {
    install(Vue) {
        Vue.component(Form.name, Form)
    }
}