import Loader from './Loader.vue'

export default {
    install(Vue) {
        Vue.component(Loader.name, Loader)
    }
}