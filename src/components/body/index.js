import Body from './Body.vue'

export default {
    install(Vue) {
        Vue.component(Body.name, Body)
    }
}