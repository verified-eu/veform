import defaultLocale from '../data/locale.json'

export const store = {

    state: {
      locale: defaultLocale,
      iso: "en_EN"
    },
    
    setLocale(locale) {
      this.state.locale = locale
    },

    setIso(iso) {
        this.state.iso = iso
    },

    getIso() {
        return this.state.iso
    }

}  