export const validateNumber = (value, constraint) => {

    if(value && !isNaN(value)) {
        return { status: true }
    }

    return {
        status: false,
        key: "validation.number"
    }

}