import { validateMin, validateMax } from "./min_max.js"
import { validateRequired } from "./required.js"
import { validateEmail } from "./email.js"
import { validateNumber } from "./number.js"
import { validateDigit } from "./digit.js"
import { validateMod11 } from "./mod11.js"
import { validateOrgNrSe, validatePNrSe, validateSNrSe, validateOnlyOrgNrSe, validatePNrOrSnrSe, validateSSNNrSe } from "./ssn_se.js"
import { validateSsnNo } from "./ssn_no"

let customValidations = {}

export const validate = (value, rules_hash) => {

    let rules = rules_hash.split("|")

    for(let rule of rules) {

        let parts = rule.split(":")

        let key = parts[0]

        let constraint
        if(parts.length > 1)
            constraint = parts[1]

        let result = { status: true }

        switch(key) {

            case "required": result = validateRequired(value, constraint); break;
            case "min": result = validateMin(value, constraint); break;
            case "max": result = validateMax(value, constraint); break;
            case "email": result = validateEmail(value, constraint); break;
            case "number": result = validateNumber(value, constraint); break;
            case "digit": result = validateDigit(value, constraint); break;
            case "mod11": result = validateMod11(value, constraint); break;
            case "orgnrse": result = validateOrgNrSe(value, constraint); break;
            case "pnrse": result = validatePNrSe(value, constraint); break;
            case "snrse": result = validateSNrSe(value, constraint); break;
            case "oorgnrse": result = validateOnlyOrgNrSe(value, constraint); break;
            case "pnrosnrse": result = validatePNrOrSnrSe(value, constraint); break;
            case "ssnse": result = validateSSNNrSe(value, constraint); break;
            case "ssnno": result = validateSsnNo(value, constraint); break;
            default: if(customValidations[key]) { result = customValidations[key](value, constraint); break; }

        }

        if(!result.args) result.args = []

        if(!result.status) return result

    }

    return { status: true }

}

export const addCustomValidation = (key, fn) => {
    customValidations[key] = fn
}