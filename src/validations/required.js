export const validateRequired = (value, constraint) => {

    if(value && value.length > 0) {
        return { status: true }
    }

    return {
        status: false,
        key: "validation.required"
    }

}