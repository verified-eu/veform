export const validateDigit = (value, constraint) => {

    if(value &&  /^\d+$/.test(value)) {
        return { status: true }
    }

    return {
        status: false,
        key: "validation.digit"
    }

}