export const validateMin = (value, constraint) => {

    if(value && value.length >= parseInt(constraint)) {
        return { status: true }
    }

    return {
        status: false,
        key: "validation.min",
        args: [constraint]
    }

}

export const validateMax = (value, constraint) => {

    if(value && value.length <= parseInt(constraint)) {
        return { status: true }
    }

    return {
        status: false,
        key: "validation.max",
        args: [constraint]
    }

}