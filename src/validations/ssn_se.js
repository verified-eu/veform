import { getIdNumberInfoSe } from '../helpers/ssn_se'

export const validateOrgNrSe = (value, constraint) => {
    let status = value && getIdNumberInfoSe(value).isOrgNumber
    return { status: status, key: "validation.orgnrse" }
}
export const validatePNrSe = (value, constraint) => {
    let status = value && getIdNumberInfoSe(value).isPersonNumber
    return { status: status, key: "validation.pnrse" }
}
export const validateSNrSe = (value, constraint) => {
    let status = value && getIdNumberInfoSe(value).isSamordningsNumber
    return { status: status, key: "validation.snrse" }
}
export const validateOnlyOrgNrSe = (value, constraint) => {
    let status = value && getIdNumberInfoSe(value).isOrgNumberAndNotPnr
    return { status: status, key: "validation.oorgnrse" }
}
export const validatePNrOrSnrSe = (value, constraint) => {
    let status = false
    if(value) {
        let IdNumberInfoSe = getIdNumberInfoSe(value)
        status = IdNumberInfoSe.isPersonNumber || IdNumberInfoSe.isSamordningsNumber
    }
    return { status: status, key: "validation.pnrosnrse" }
}
export const validateSSNNrSe = (value, constraint) => {
    let status = value && getIdNumberInfoSe(value).controlNumberValid
    return { status: status, key: "validation.ssnse" }
}