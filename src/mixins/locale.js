import { store } from '../store'

export default {
	
	data() {
		return {
			vState: store.state
		}
	},
	
	methods: {
		
		__(key, ...args) {
			
			if(!this.vState || !this.vState.locale)
				return ""
			
			if(!this.vState.locale[key] || !this.vState.locale[key][this.vState.iso])
				return `${ key }.${ this.vState.iso }`
			
			let str = this.vState.locale[key][this.vState.iso]
			
			for(let arg of args)
				str = str.replace('%s', arg)
			
			return str
		},
		
		__exists(key) {
			
			if(!this.vState || !this.vState.locale)
				return false
			
			return !!(this.vState.locale[key] && this.vState.locale[key][this.vState.iso]);
		},
		
		mergeLocale(customLocale) {
			store.setLocale({ ...this.vState.locale, ...customLocale })
		},
		
		getLocale() {
			return this.vState && this.vState.locale || {}
		},
		
		setIso(iso) {
			store.setIso(iso)
		},
		
		getIso() {
			return store.getIso()
		}
		
	}
}