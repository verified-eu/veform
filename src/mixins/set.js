import Vue from "vue"

export default {

    methods: {

        set(object, key, value) {

            Vue.set(object, key, value)

        }

    }

}