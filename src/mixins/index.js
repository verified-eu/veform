import set from './set'
import locale from './locale'

export const addMixins = (Vue) => {

    Vue.mixin(set)
    Vue.mixin(locale)

}