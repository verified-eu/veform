import { mount } from './components'
import { addMixins } from './mixins'
import { addCustomValidation } from './validations'
import { VueMaskDirective } from 'v-mask'
import helpers from './helpers'

const veform = {

    install(Vue, options = {}) {

        Vue.directive('mask', VueMaskDirective)

        addMixins(Vue)
        mount(Vue)

    },

    addCustomValidation,

    helpers

}

export default veform
